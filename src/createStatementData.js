const ComedyCalculator = require('./calculator/ComedyCalculator')
const TragedyCalculator = require('./calculator/TragedyCalculator')

const  createPerformanceCalculator = (aPerformance, aPlay) => {
  switch(aPlay.type) {
  case 'tragedy': return new TragedyCalculator(aPerformance, aPlay)
  case 'comedy' : return new ComedyCalculator(aPerformance, aPlay)
  default:
    throw new Error(`unknown type: ${aPlay.type}`)
  }
}

const totalAmount = (data) => {
  return data.performances.reduce((acc, perf) => acc + perf.amount, 0)
}

function totalVolumeCredits(data) {
  return data.performances
    .reduce(
      (acc, aPerformance) => acc + aPerformance.volumeCredits,
      0)
}

const createStatementData = (invoice, plays) => {
  const statementData = {}
  statementData.customer = invoice.customer
  function playFor(aPerformance) {
    return plays[aPerformance.playID]
  }

  const enrichPerformance = aPerformance => {
    const calculator = createPerformanceCalculator(aPerformance, playFor(aPerformance))
    const result = Object.assign({}, aPerformance)
    result.play = calculator.play
    result.amount = calculator.amount
    result.volumeCredits = calculator.volumeCredits
    return result
  }
  statementData.performances = invoice.performances.map(enrichPerformance)
  statementData.totalAmount = totalAmount(statementData)
  statementData.totalVolumeCredits = totalVolumeCredits(statementData)
  return statementData
}

module.exports = { createStatementData }