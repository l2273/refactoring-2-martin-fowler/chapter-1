[![pipeline status](https://gitlab.com/refactoring-2-martin-fowler/chapter-1/badges/master/pipeline.svg)](https://gitlab.com/refactoring-2-martin-fowler/chapter-1/-/commits/master)
[![coverage](https://gitlab.com/refactoring-2-martin-fowler/chapter-1/badges/master/coverage.svg)](https://gitlab.com/refactoring-2-martin-fowler/chapter-1/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=refactoring.chapter1&metric=alert_status)](https://sonarcloud.io/dashboard?id=refactoring.chapter1)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=refactoring.chapter1&metric=coverage)](https://sonarcloud.io/dashboard?id=refactoring.chapter1)


## Chapter 1

This is the chapter one of the Martin Fowler's **Refactoring Book**.

* Installation:

```shell script
yarn install
```

* Testing:

```shell script
yarn test
yarn coverage
yarn sonar
```